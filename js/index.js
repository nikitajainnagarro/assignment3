let students = [];

function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)+@\w+([\.:]?\w+)+(\.[a-zA-Z0-9]{2,3})+$/;
    if (inputText.match(mailformat)) {
        for (let student of students) {
            if (inputText == student.email) {
                alert("Email already registered!");
                document.forms["enroll-form"]["email"].focus();
                return false;
            }
        }
        return true;
    } else {
        alert("You have entered an invalid email address!");
        document.forms["enroll-form"]["email"].focus();
        return false;
    }
}

function ValidateURL(inputText) {
    var urlformat = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    if (inputText.match(urlformat)) {
        return true;
    } else {
        return false;
    }
}

function checkFormFill() {
    let name = document.forms["enroll-form"]["name"].value;
    let email = document.forms["enroll-form"]["email"].value;
    let website = document.forms["enroll-form"]["website"].value;
    let imageLink = document.forms["enroll-form"]["imageLink"].value;
    let gender = document.forms["enroll-form"]["gender"].value;
    let skills = [];
    if (document.getElementById("java").checked) {
        skills.push(document.getElementById("java").value);
    }
    if (document.getElementById("html").checked) {
        skills.push(document.getElementById("html").value);
    }
    if (document.getElementById("css").checked) {
        skills.push(document.getElementById("css").value);
    }
    if (name == null || name == "") {
        alert("Please provide name");
        document.forms["enroll-form"]["name"].focus();
        return false;
    } else if (email == null || email == "") {
        alert("Please provide email");
        document.forms["enroll-form"]["email"].focus();
        return false;
    } else if (!ValidateEmail(email)) {
        return false;
    } else if (website == null || website == "") {
        alert("Please provide website");
        document.forms["enroll-form"]["website"].focus();
        return false;
    } else if (!ValidateURL(website)) {
        alert("You have entered an invalid url for website!");
        document.forms["enroll-form"]["website"].focus();
        return false;
    } else if (imageLink == null || imageLink == "") {
        alert("Please provide image link");
        document.forms["enroll-form"]["imageLink"].focus();
        return false;
    } else if (!ValidateURL(imageLink)) {
        alert("You have entered an invalid url for imageLink!");
        document.forms["enroll-form"]["imageLink"].focus();
        return false;
    } else if (gender == null || gender == "") {
        alert("Please choose gender");
        return false;
    } else if (skills.length == 0) {
        alert("Please choose skill(s)");
        return false;
    } else {
        student = {};
        student.name = name;
        student.email = email;
        student.website = website;
        student.imageLink = imageLink;
        student.gender = gender;
        student.skills = skills;
        enrollStudent(student);
    }
}

function enrollStudent(student) {
    let table = document.getElementById("student-detail-table");
    let row = table.insertRow(-1);
    row.insertCell(0).innerHTML =
        "<b>" +
        student.name +
        "</b><br>" +
        student.gender +
        "<br>" +
        student.email +
        "<br><a target='_blank' href =" +
        student.website +
        ">" +
        student.website +
        "</a><br>" +
        student.skills +
        "<br>";
    row.insertCell(1).innerHTML =
        '<a target="_blank" href=' +
        student.imageLink +
        "><img src=" +
        student.imageLink +
        ' alt="Student Photo"></a>';
    students.push(student);
    document.getElementById("enroll-form").reset();
}